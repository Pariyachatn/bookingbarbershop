import firebase from "firebase/app";

import "firebase/firestore";
import "firebase/auth";
import "firebase/storage";

const firebaseConfig = {
  apiKey: "AIzaSyCJGQPJcol_YkEodmbvjYjL0neX_qZOheY",
  authDomain: "barberbooking-6aa1d.firebaseapp.com",
  databaseURL: "https://barberbooking-6aa1d.firebaseio.com",
  projectId: "barberbooking-6aa1d",
  storageBucket: "barberbooking-6aa1d.appspot.com",
  messagingSenderId: "303265710892",
  appId: "1:303265710892:web:eff2764c6aec09aa3be289",
  measurementId: "G-SLJSCKC1QE",
};

// Get a Firestore instance
firebase.initializeApp(firebaseConfig);

const db = firebase.firestore();
const auth = firebase.auth();
const storage = firebase.storage();

export { firebase, db, auth, storage };
