import Vue from "vue";
import Vuex from "vuex";
import {auth} from "@/firebase";
Vue.use(Vuex);
export const store = new Vuex.Store({
  state: {
    isLoading: false,
    user: {
      loggedIn: false,
      data: null,
    },
  },
  getters: {
    user(state) {
      return state.user;
    },
  },
  mutations: {
    SET_LOGGED_IN(state, value) {
      state.user.loggedIn = value;
    },
    SET_USER(state, data) {
      state.user.data = data;
    },
    SET_LOADING(state, value) {
      state.isLoading = value;
    },
  },
  actions: {
    SignIn({ commit }, payload) {
      commit("SET_LOADING", true);
      auth.signInWithEmailAndPassword(payload.email, payload.password).then(
        (user) => {
          commit("SET_LOADING", false);
          commit("SET_LOGGED_IN", true);
          commit("SET_USER", user);
          console.log(this.getters.user);
        },
        (err) => {
          commit("SET_LOADING", false);
          commit("SET_LOGGED_IN", false);
          console.log(this.getters.user);
        }
      );
    },
    SignOut({commit}){
        auth.signOut().then(()=>{
            commit("SET_LOGGED_IN", false);
            commit("SET_USER", null);
        },err=>{
            console.log(err);
        });
    },
    autoSignIn({commit},payload){
        commit("SET_LOGGED_IN", true);
        commit("SET_USER", payload);
    }
  },

  // actions: {
  //   fetchUser({ commit }, user) {
  //     commit("SET_LOGGED_IN", user !== null);
  //     if (user) {
  //       commit("SET_USER", {
  //         displayName: user.displayName,
  //         email: user.email
  //       });
  //     } else {
  //       commit("SET_USER", null);
  //     }
  //   }
  // }
});
