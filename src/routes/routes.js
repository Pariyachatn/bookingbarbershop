import DashboardLayout from "@/layout/DashboardLayout.vue";
// GeneralViews
import NotFound from "@/pages/NotFoundPage.vue";

// Admin pages
import Overview from "@/pages/Overview.vue";

import ServiceManagement from "@/pages/ServiceManagement.vue";
import ServiceDetail from "@/pages/ServiceDetail.vue";
import BarberDetail from "@/pages/BarberDetail.vue";

import ProductManagement from "@/pages/ProductManagement.vue";
import ProductDetail from "@/pages/ProductDetail.vue";

import YourWork from "@/pages/YourWork.vue";

import Login from "@/pages/Login.vue";

const routes = [
  {
    path: "/",
    component: DashboardLayout,
    redirect: "/admin/yourwork",
  },
  {
    path: "/admin",
    component: DashboardLayout,
    redirect: "/admin/overview",
    meta: { requiresAuth: true },
    name:"home",
    children: [
      {
        path: "overview",
        name: "Overview",
        component: Overview,
      },
      {
        path: "yourwork",
        name: "YourWork",
        component: YourWork,
      },
      {
        path: "service_management",
        name: "ServiceManagement",
        component: ServiceManagement,
      },
      {
        path: "service_management/create",
        name: "ServiceManagementCreate",
        component: ServiceDetail,
      },
      {
        path: "service_management/Detail/:id",
        name: "ServiceManagementDetail",
        component: ServiceDetail
      },
      {
        path: "service_management/Detail/:serviceid/barber/create",
        name: "BarberCreate",
        component: BarberDetail,
      },
      {
        path: "service_management/Detail/:serviceid/barber/Detail/:id",
        name: "BarberDetail",
        component: BarberDetail,
      },
      {
        path: "product_management/",
        name: "ProductManagement",
        component: ProductManagement,
      },
      {
        path: "product_management/create",
        name: "ProductCreate",
        component: ProductDetail,
      },
      {
        path: "product_management/detail/:id",
        name: "ProductDetail",
        component: ProductDetail,
      }
    ],
  },
  {
    path: "/login",
    meta: { requiresAuth: false },
    component: Login,
    name:"Login"
  },
  { path: "*", component: NotFound },
];

/**
 * Asynchronously load view (Webpack Lazy loading compatible)
 * The specified component must be inside the Views folder
 * @param  {string} name  the filename (basename) of the view to load.
function view(name) {
   var res= require('../components/Dashboard/Views/' + name + '.vue');
   return res;
};**/

export default routes;
